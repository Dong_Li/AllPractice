<%-- 
    Document   : ckdd
    Created on : 2018-6-20, 19:01:05
    Author     : Administrator
--%>

<%@page import="bean.PageBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>查看订单</title>
    </head>
    
    <body style="background-image:url(image/car1.jpg)">
        <%
            if(request.getSession().getAttribute("id")==null) request.getRequestDispatcher("login.jsp").forward(request, response);
        %>
        
        <h2 align="center">订单信息</h2>
        <table border="1" align="center" width="50%">
            <tr><th>用户id</th><th>名字</th><th>价格</th><th>电话</th><th>地址</th></tr>
            <c:forEach var="order" items="${pageBean.data}" varStatus="vs">
                
                <tr>
                    <td align="center"><c:out value="${order.id}"/></td>
                    <td align="center"><c:out value="${order.name}"/></td>
                    <td align="center"><c:out value="${order.price}"/></td>
                    <td align="center"><c:out value="${order.tel}"/></td>
                    <td align="center"><c:out value="${order.addr}"/></td>
                </tr>
            </c:forEach>
        </table>
        
                <p align="center">
            每页${pageBean.pageSize}行
            共${pageBean.totalRows}行
            页数${pageBean.curPage}/${pageBean.totalPages}
            <br/>
            <c:choose>
                <c:when test="${pageBean.curPage==1}">首页 上一页</c:when>
                <c:otherwise>
                    <a href="ckdd?page=1">首页</a>
                    <a href="ckdd?page=${pageBean.curPage-1}">上一页</a>
                </c:otherwise>
            </c:choose>
                <c:choose>
                    <c:when test="${pageBean.curPage==pageBean.totalPages}">下一页 尾页</c:when>
                    <c:otherwise>
                        <a href="ckdd?page=${pageBean.curPage+1}">下一页</a>
                        <a href="ckdd?page=2">尾页</a>
                    </c:otherwise>
                </c:choose>
        </p>
        
    <center>输入要删除订单的用户id：<input id="id" type="text" placeholder="收货成功即可删除订单"><input id="del" type="button" value="删除"><a href="admin.jsp">返回</a></center>
        
        
        <script>
			var del=document.getElementById("del");
			var id=document.getElementById("id");
			
			del.addEventListener("click",function(){
				window.location.href="delorder?id="+id.value;
				},false);
			
        </script>
        
    </body>
</html>
