<%-- 
    Document   : registersuc
    Created on : 2018-6-16, 14:44:58
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>注册成功</title>
    </head>
    <body onload="returnUrlByTime()">
    <center><h2>注册成功！<span id="layer">5</span>秒后返回主页</h2></center>
        <%
            response.setHeader("Refresh", "5;URL=index.jsp");
        %>
    
        <script>
        var time=6;
        function returnUrlByTime()
        {
            window.setTimeout('returnUrlByTime()',1000);
            
            time=time-1;
            
            document.getElementById("layer").innerHTML=time;
        }
    </script>
    
    </body>
</html>
