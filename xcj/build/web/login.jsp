<%-- 
    Document   : login
    Created on : 2018-6-14, 20:11:18
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>登录界面</title>
    </head>
    
<style>
#login input{
	width:270px;
	height:40px;
	margin-top:15px;
	text-indent:40px;
	border:solid 1px #C8C8C8;
	}
#login span{
	display:inline-block;
	width:135px;
	height:40px;
	background-color:#CCC;
	text-align:center;
	margin-top:20px;
	line-height:40px;
	font-weight:bold;
	color:white;
	}
#login span:hover{
	background-color:#666;
	cursor:pointer;
	}
#wei ul{
	list-style:none;
	}
#wei ul li{
	float:left;
	line-height:25px;
	padding:0px 10px;
	font-size:14px;
	border-right:solid 1px rgba(204,204,204,1);
	}
#wei ul li a{
	text-decoration:none;
	}

#sub,#register:hover{
	cursor:pointer;
	}

</style>
    
    <body style="min-width:1300px;">
    
    
        <div style=" display:inline-block; float:left;width:500px; height:400px;margin-left:10%; margin-top:10%;">
        	<img src="image/login1.jpg">
        </div>
        
        
        <div id="login" style="	display:inline-block; margin-top:8%; margin-left:180px; width:280px; height:300px; padding:10px 10px; float:left;">
            <form action="logincheck" method="post">
            	<table style="text-align:right;">
                	<tr><td><input autocomplete="off" name="name" id="name" style="background-image:url(image/login3.png); background-size:40px 40px; background-repeat:no-repeat;" type="text" placeholder="用户名"></td></tr>
                    <tr><td><input name="pw" id="pw" style="background-image:url(image/login2.png); background-size:40px 40px; background-repeat:no-repeat;" type="password" placeholder="密码"></td></tr>
                    <input name="iden" id="iden" type="hidden" value="">
                    <tr><td><input id="code1" name="code1" placeholder="验证码" style=" margin-right:10px;width:180px;" type="text"><img src="checkcode" width="80" height="45" style="vertical-align:middle;" onclick="self.location.reload()"></td></tr>
                    <tr><td colspan="2"><span id="user">用户</span><span id="admin">管理员</span></td></tr>
                    
                    
                    <tr><td><input id="sub" style="text-indent:0px; font-size:20px; color:white; background-color:#00BB7E;" type="submit" value="登录"></td></tr>
                    
                    <tr><td><input id="register" onClick="location='register.jsp'" style="text-indent:0px; font-size:20px; color:white; background-color:#00BB7E;" type="button" value="注册"></td></tr>
                    
                </table>
            </form>
        </div>
        
        <div id="wei" style="width:900px; height:60px; display:block;position:absolute;bottom: 0px;right: 300px; margin-left:15%; border-top:solid 1px rgba(204,204,204,0.8); border-bottom:solid 1px rgba(204,204,204,0.8);">
        	<ul>
            	<li><a href="">关于我们</a></li>
                <li><a href="">我们承诺</a></li>
                <li><a href="">加入我们</a></li>
                <li><a href="">商家入驻</a></li>
                <li style="margin-left:100px;"><a href=""><img style="vertical-align:middle;" src="image/login4.png">七天退</a></li>
                <li><a href=""><img style="vertical-align:middle;" src="image/login5.png">质保退</a></li>
                <li><a href=""><img style="vertical-align:middle;" src="image/login6.png">运费险</a></li>
            </ul>
        </div>
        
<script>

var user=document.getElementById("user");
var admin=document.getElementById("admin");
var iden=document.getElementById("iden");
var sub=document.getElementById("sub");
var code1=document.getElementById("code1");
var name=document.getElementById("name");
var pw=document.getElementById("pw");

user.addEventListener("click",function(){
	user.style.backgroundColor="#666";
	iden.value="user";
	admin.style.backgroundColor="#CCC";
	},false);

admin.addEventListener("click",function(){
	admin.style.backgroundColor="#666";
	iden.value="admin";
	user.style.backgroundColor="#CCC";
	},false);

sub.addEventListener("click",function(){
	if(code1.value===""||name.value===""||pw.value===""||iden.value==="") alert("信息不能为空！");
	},false);

</script>
        
    </body>
</html>
