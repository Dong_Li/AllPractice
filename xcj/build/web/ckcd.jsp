<%-- 
    Document   : ckcd
    Created on : 2018-6-20, 19:01:35
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>查看商品</title>
    </head>
    <body style="background-image:url(image/car1.jpg)">
        <%
            if(request.getSession().getAttribute("id")==null) request.getRequestDispatcher("login.jsp").forward(request, response);
        %>
        
        <h2 align="center">商品信息</h2>
        <table border="1" align="center" width="50%">
            <tr><th>商品名</th><th>价格</th><th colspan="2">操作</th></tr>
            <c:forEach var="menu" items="${pageBean.data}" varStatus="vs">
                
                <tr>
                    <td align="center"><c:out value="${menu.name}"/></td>
                    <td align="center"><c:out value="${menu.price}"/></td>
                    <td align="center"><a href="delmenu?name=${menu.name}">删除</a></td>
                    <td align="center"><a href="modifymenu.jsp?name=${menu.name}&price=${menu.price}">修改</a></td>
                </tr>
            </c:forEach>
        </table>
        
                <p align="center">
            每页${pageBean.pageSize}行
            共${pageBean.totalRows}行
            页数${pageBean.curPage}/${pageBean.totalPages}
            <br/>
            <c:choose>
                <c:when test="${pageBean.curPage==1}">首页 上一页</c:when>
                <c:otherwise>
                    <a href="ckcd?page=1">首页</a>
                    <a href="ckcd?page=${pageBean.curPage-1}">上一页</a>
                </c:otherwise>
            </c:choose>
                <c:choose>
                    <c:when test="${pageBean.curPage==pageBean.totalPages}">下一页 尾页</c:when>
                    <c:otherwise>
                        <a href="ckcd?page=${pageBean.curPage+1}">下一页</a>
                        <a href="ckcd?page=2">尾页</a>
                    </c:otherwise>
                </c:choose><a href="admin.jsp">返回</a>
        </p>
        
        
   
        
        
    </body>
</html>
