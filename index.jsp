<%-- 
    Document   : index
    Created on : 2018-6-10, 13:23:10
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jstl/sql_rt" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<sql:setDataSource var="ds" driver="com.mysql.jdbc.Driver" user="root" password="" url="jdbc:mysql://localhost:3306/xcj"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width,innitial-scale=1.0"/>
        <title>乡村街</title>
    </head>

<style>
*{
	padding:0px;
	margin:0px;
	}
body{
	background-color: white;
	}
#dh{
	width:100%;
	height:35px;
	background-color:#EEE;
	box-shadow:0px 3px 15px 2px #999999;
	}
#dh ul{
	list-style:none;
	width:200px;
	margin-left:10%;
	font-size:13px;
	color:#666;
	}
#dh ul:first-child li{
	float:left;
	margin-left:10px;
	margin-top:7px;
	}
#dh ul:first-child li:first-child:hover{
	cursor:pointer;
	}
#dh ul:first-child li:nth-child(3):hover{
	text-decoration:underline;
	}

#dh #wdshoucang{
	float:right;
	width:100px;
	height:20px;
	right:220px;
	padding-top:7px;
	position:absolute;
	text-align:center;
	}
#dh #wdshoucang:hover{
	color:#1FDAA7;
	width:100px;
	border:solid 1px #CCCCCC;
	background-color:white;
	height:120px; 
	}	
#dh ul:nth-child(2) li:first-child ul li{/*我的乡村街 下拉菜单 里面的li的样式*/
	margin-top:7px;
	height:22px;
	}
#dh ul:nth-child(2) li:first-child ul li:hover{
	background-color:#EAF3FD;
	}
#dh ul:nth-child(2) li:first-child ul li a{
	color:#666;
	}
#dh ul:nth-child(2) li:first-child ul li a:hover{
	color:#1FDAA7;
	}

	
/*第二个div*/
#sousuo{
	width:85%;
	margin:0 auto;
	height:110px;
	/*background-color:green;*/
	margin-top:15px;
	}	
#ssbtn{
	background-color:#00BB7E;  /*#06F*/
	width:60px;
	margin-top:10px;
	height:46px;
	line-height:30px;
	float:left;
	text-align:center;
	}
#ssbtn input{
	background-color:#00BB7E;
	width:60px;
	height:46px;
	border:none;
	color:white;
	font-size:16px;
	}
#ssbtn input:hover{
	cursor:pointer;
	}
#ssbtn:hover{
	cursor:pointer;
	}
	
/*第三个div*/
#neirong{
	width:1200px;
	margin:0 auto;;
	height:2000px;
	border:solid 1px #06F;
	}
#nrdh{
	width:100%;
	height:50px;
	background-color:#00BB7E;  /*#06F*/
	}
#nrdh ul{
	height:100%;
	width:80%;
	margin:0 auto;
	list-style:none;
	}
#nrdh ul li{
	float:left;
	text-align:center;
	color:white;
	padding:10px 50px;
	font-size:20px;
	height:30px;
	border-left:solid 1px white;
	}
#nrdh ul li:hover{
	background-color:#48D5A0;
	cursor:pointer;
	}
	
/*大床界面*/
#hanbaojm img{
	width:250px;
	height:200px;
	}
#hanbaojm div{
	background-color:#F6F6F6;
	float:left;
	border:solid 1px #CCCCCC;
	margin-left:30px;
	margin-top:20px;
	}
#hanbaojm div:hover{
	border:solid 1.5px #FF8000;
	}
#hanbaojm span{
	display:block;
	padding-left:10px;
	}
#hanbaojm a{
	text-decoration:none;
	background-color:#FF0909;
	font-style:italic;
	color:white;
	margin-left:20px;
	}
	
/*衣服界面*/
#zhajijm img{
	width:250px;
	height:200px;
	}
#zhajijm div{
	background-color:#F6F6F6;
	float:left;
	border:solid 1px #CCCCCC;
	margin-left:30px;
	margin-top:20px;
	}
#zhajijm div:hover{
	border:solid 1.5px #FF8000;
	}
#zhajijm span{
	display:block;
	padding-left:10px;
	}
#zhajijm a{
	text-decoration:none;
	background-color:#FF0909;
	font-style:italic;
	color:white;
	margin-left:20px;
	}
	
/*电脑界面*/
#yinliaojm img{
	width:250px;
	height:200px;
	}
#yinliaojm div{
	background-color:#F6F6F6;
	float:left;
	border:solid 1px #CCCCCC;
	margin-left:30px;
	margin-top:20px;
	}
#yinliaojm div:hover{
	border:solid 1.5px #FF8000;
	}
#yinliaojm span{
	display:block;
	padding-left:10px;
	}
#yinliaojm a{
	text-decoration:none;
	background-color:#FF0909;
	font-style:italic;
	color:white;
	margin-left:20px;
	}
	
/*手机界面*/
#bqljm img{
	width:250px;
	height:200px;
	}
#bqljm div{
	background-color:#F6F6F6;
	float:left;
	border:solid 1px #CCCCCC;
	margin-left:30px;
	margin-top:20px;
	}
#bqljm div:hover{
	border:solid 1.5px #FF8000;
	}
#bqljm span{
	display:block;
	padding-left:10px;
	}
#bqljm a{
	text-decoration:none;
	background-color:#FF0909;
	font-style:italic;
	color:white;
	margin-left:20px;
	}
	
	
/*首页界面*/
/*#shouyejm ul{
	margin-left:400px;
	margin-top:-30px;
	}
#shouyejm ul li{
	list-style:none;
	display:inline-block;
	
	text-align:center;
	font-weight:bold;
	}*/
	
/*公告栏*/
#ggl{
	width:100%;
	height:30px;
	list-style:none;
	text-align:center;
	background-color:#00BB7E;
	}
#ggl li{
	width:32.5%;
	float:left;
	height:30px;
	}
#ggl li span{
	padding-bottom:3px;
	/*border-bottom:2px solid white;*/
	color:white;
	}
#ggl li span:hover{
	border-bottom:2px solid white;
	cursor:default;
	}	
#gglnr1 a{
	display:block;
	}
#gglnr1 a:nth-child(2n){
	color:#CF3;
	}
#gglnr1 a:nth-child(2n+1){
	color:#3FF;
	}
	
	
#rightplug div:hover{
	background-color:#CCC; 
	}
	
/*热卖界面*/
#remai img{
	width:250px;
	height:200px;
	}
#remai div{
	background-color:#F6F6F6;
	float:left;
	border:solid 1px #CCCCCC;
	margin-left:30px;
	margin-top:20px;
	}
#remai div:hover{
	border:solid 1.5px #FF8000;
	}
#remai span{
	display:block;
	padding-left:10px;
	}
#remai a{
	text-decoration:none;
	background-color:#FF0909;
	font-style:italic;
	color:white;
	margin-left:20px;
	}

</style>    
    
<body style="min-width:1100px;" onLoad="ss()">
    
	<!-- 导航div-->
    <div id="dh">
    <!-- 第一个ul-->
    	<ul>
        	<li id="shoucang"><img id="aixin" style="width:15px;height:13px; vertical-align:middle;" src="image/aixin1.png">&nbsp;收藏乡村街</li>
            <li><a id="login" href="login.jsp" style="text-decoration:none; color:#E88602;">登录</a></li>
            <li><a id="register" href="register.jsp" style="text-decoration:none;color:#666;">注册</a></li>
        </ul>
        
        <!-- 第二个ul-->
        <ul id="wdshoucang" onMouseOver="displaymenu()" onMouseOut="hiddenmenu()">
        	<li ><a>我的乡村街</a>
            	<ul id="menu1" style="display:none; width:100px; height:130px;float:left; margin-top:0px; background-color:white; position:absolute; right:0px; margin-left:0px; padding-top:0px;">
        		  <li><a style="text-decoration:none; vertical-align:middle;" href="myaccount">我的帐户</a></li>
                          <li><a style="text-decoration:none; vertical-align:middle;" href="ckdd">我的账单</a></li>
           		  <li><a style="text-decoration:none; vertical-align:middle;" href="">我的评价</a></li>
           		  <li><a style="text-decoration:none;vertical-align:middle;" href="">我的收藏</a></li>
        		</ul>
            </li>
        </ul>
        
    </div>
    
    <!-- 搜索div -->
    <div id="sousuo">
    	<img src="image/乡村街logo.png" width="220" height="200" style="float:left;">
        <form action="login.jsp"><input type="text" style="margin-top:10px; margin-left:15%; float:left; height:40px ; width:500px; border:solid #00BB7E; text-indent:10px;" placeholder="购物狂欢就在乡村街">
        <div id="ssbtn"><input type="submit" value="搜索"></div></form>
        <div style="display:block; width:500px; height:50px;  float:left; line-height:50px; margin-left:15%; font-size:10px;">
        	<a style="text-decoration:none;" href="https://uland.taobao.com/sem/tbsearch?refpid=mm_26632258_3504122_32538762&clk1=de693d662f7fab7427407fa103319eb1&keyword=%E7%94%B5%E8%84%91&page=0">高配电脑</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a style="text-decoration:none;" href="">高级大床</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a style="text-decoration:none;" href="">高档衣服</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a style="text-decoration:none;" href="https://uland.taobao.com/sem/tbsearch?refpid=mm_26632258_3504122_32538762&clk1=de693d662f7fab7427407fa103319eb1&keyword=%E6%89%8B%E6%9C%BA&page=0">高价手机</a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a style="text-decoration:none;" href="">高原零食</a>
        </div>
        
    </div><!-- 搜索div结束 -->
        
    <!-- 内容div -->
    <div id="neirong">
    	<!-- 内容导航div -->
    	<div id="nrdh">
        	<ul>
            	<li id="shouye">首页</li>
            	<li id="hanbao">大床</li>
                <li id="zhaji">衣服</li>
                <li id="yinliao">电脑</li>
                <li id="bql" style="border-right:solid 1px white;">手机</li>
            </ul>
        </div>
        
         <!-- 床分类界面-->
        <sql:query var="hbresults" sql="select * from menu where name like '%大床%'" dataSource="${ds}"/>
        <div id="hanbaojm" style="width:100%;height:80px; display:none;">
                    <c:forEach var="row" items="${hbresults.rows}">
                        <div><img src="${row.image}"><span>${row.name}</span><span>销量：${row.salec}</span><span style="color:#F88216; float:left; margin-left:5px; font-size:24px;">¥${row.price}</span><span style="float:left; margin-top:6px; ">/套</span><br><a href="menuadd.jsp?name=${row.name}&price=${row.price}">开始购买</a></div>
                    </c:forEach>
        </div>
        
          <!-- 衣服界面-->
        <sql:query var="zjresults" sql="select * from menu where name like '%衣%'  or name like '%服%'" dataSource="${ds}"/>
        <div id="zhajijm" style="width:100%;height:80px; display:none;">
        	<c:forEach var="row" items="${zjresults.rows}">
                        <div><img src="${row.image}"><span>${row.name}</span><span>销量：${row.salec}</span><span style="color:#F88216; float:left; margin-left:5px; font-size:24px;">¥${row.price}</span><span style="float:left; margin-top:6px; ">/件</span><br><a href="menuadd.jsp?name=${row.name}&price=${row.price}">开始购买</a></div>
                    </c:forEach>
        </div>
        
         <!-- 电脑界面-->
        <sql:query var="ylresults" sql="select * from menu where name like '%电脑%' " dataSource="${ds}"/>
        <div id="yinliaojm" style="width:100%;height:80px; display:none;">
        	<c:forEach var="row" items="${ylresults.rows}">
                        <div><img src="${row.image}"><span>${row.name}</span><span>销量：${row.salec}</span><span style="color:#F88216; float:left; margin-left:5px; font-size:24px;">¥${row.price}</span><span style="float:left; margin-top:6px; ">/台</span><br><a href="menuadd.jsp?name=${row.name}&price=${row.price}">开始购买</a></div>
                    </c:forEach>
        </div>
        
         <!-- 手机界面-->
        <sql:query var="bqlresults" sql="select * from menu where name like '%手机%' " dataSource="${ds}"/>
        <div id="bqljm" style="width:100%;height:80px; display:none;">
        <c:forEach var="row" items="${bqlresults.rows}">
                        <div><img src="${row.image}"><span>${row.name}</span><span>销量：${row.salec}</span><span style="color:#F88216; float:left; margin-left:5px; font-size:24px;">¥${row.price}</span><span style="float:left; margin-top:6px; ">/台</span><br><a href="menuadd.jsp?name=${row.name}&price=${row.price}">开始购买</a></div>
                    </c:forEach>
        </div>
          
        <!-- 首页界面-->
        <div id="shouyejm" style=" width:80%; margin:0 auto; width:100%; height:700px; padding:5px 5px; box-sizing:border-box;display:block;">
       
        	<div style=" display:inline-block; height:30px; background-color:blue; width:500px; float:left; height:300px; margin-left:50px;">
        	<img width="500" height="300" id="shouyepic" src="image/首页1.jpg">
            </div>
            
             <!--公告栏-->
             <div style=" box-sizing:border-box;display:block; float:left;height:300px; margin-left:5px; border:2px solid #00BB7E;width:550px;">
        	<ul id="ggl">
            	<li><span id="dztj">店长推荐</span></li>
                <li><span id="xwzx">新闻中心</span></li>
                <li><span id="gywm">关于我们</span></li>
                
            </ul>
                <!--公告栏内容-->
            	<div id="gglnr1" style=" padding:10px;box-sizing:border-box;background-color:#666;line-height:30px;display:block; margin:0 auto; width:100%;height:268px;">
                	<a>运动服+运动鞋+运动裤 更豪爽！</a><a>家具+床上用品+生活用品 更实惠！</a><a>全包邮</a>
                </div>
                <div id="gglnr2" style=" display:none;margin:0 auto; margin-top:10px;width:95%;height:250px;">
                新闻中心
                </div>
                <div id="gglnr3" style=" display:none;margin:0 auto; margin-top:10px;width:95%;height:250px;">
                关于我们
                </div>
            </div>
            
           <!-- 时钟画布、购物车 -->
           		<div id="rightplug" style="width:100px;height:500px; background-color:#4F4F4F; border-radius:10px; display:block;float:right; position:fixed; right:10px; top:80px;">
        			<canvas id="cvs" width="100" height="100" style="margin:0 auto;margin-top:0px;"></canvas>
       				 <span id="cfts" style=" margin:5px auto; text-align:center;width:100px;display:block; color:white; "></span>
        			 <div id="gwcar" style="width:100px;height:50px;display:block; border-bottom:1px white solid;text-align:center;color:white; padding-top:0px;">
                                     <form action="carservlet">
    	<input style="width:100%;height:50px; color:white; background-color:rgba(0,0,0,0); font-size:17px; border:none;" type="submit" value="购物车">
    </form>
    				</div>
                     <div style="width:100px;height:50px;display:block; border-bottom:1px white solid;text-align:center;color:white; padding-top:20px;">礼品盒</div>
                     <div style="width:100px;height:50px;display:block; text-align:center;color:white; padding-top:20px;">抽奖中心</div>
        		</div>
                
                <!--热卖-->
                <sql:query var="results" sql="select * from menu" dataSource="${ds}"/>
                
                <div id="remai" style="width:1185px;height:500px; float:left; display:inline-block; margin-top:10px; margin-left:1px;">
                <c:forEach var="row" items="${results.rows}">
                        <div><img src="${row.image}"><span>${row.name}</span><span>销量：${row.salec}</span><span style="color:#F88216; float:left; margin-left:5px; font-size:24px;">¥${row.price}</span><span style="float:left; margin-top:6px; ">/份</span><br><a href="menuadd?name=${row.name}&price=${row.price}">开始购买</a></div>
                    </c:forEach>
                	
                </div>
            
            
        </div><!--首页界面结束-->
        
        <!-- 搜索得到的界面-->
        <div id="getjm" style="background-color:blue; width:70%;height:80px; display:none;">搜索得到的界面<%= request.getParameter("x")%></div><!--搜索界面结束-->
        
    </div><!--内容div结束-->
    
    
</body>

<script>
//导航的js
var shoucang=document.getElementById("shoucang");
var aixin=document.getElementById("aixin");
shoucang.addEventListener("mouseover",function(){
	aixin.src="image/aixin2.png";
	},false);
shoucang.addEventListener("mouseout",function(){
	aixin.src="image/aixin1.png";
	},false);
	
function displaymenu(){
	var menu=document.getElementById("menu1");
	menu.style.display="block";
	}
function hiddenmenu(){
	var menu=document.getElementById("menu1");
	menu.style.display="none";
	}
	
var hanbao=document.getElementById("hanbao");
var zhaji=document.getElementById("zhaji");
var yinliao=document.getElementById("yinliao");
var bql=document.getElementById("bql");
var ssbtn=document.getElementById("ssbtn");
var shouye=document.getElementById("shouye");   //得到首页对象

var hanbaojm=document.getElementById("hanbaojm");
var zhajijm=document.getElementById("zhajijm");
var yinliaojm=document.getElementById("yinliaojm");
var bqljm=document.getElementById("bqljm");
var getjm=document.getElementById("getjm");
var shouyejm=document.getElementById("shouyejm"); //得到首页界面对象

hanbao.addEventListener("click",function(){
	zhajijm.style.display="none";
	yinliaojm.style.display="none";
	bqljm.style.display="none";
	hanbaojm.style.display="block";
	shouyejm.style.display="none";
	getjm.style.display="none";
	},false);
zhaji.addEventListener("click",function(){
	hanbaojm.style.display="none";
	yinliaojm.style.display="none";
	bqljm.style.display="none";
	zhajijm.style.display="block";
	shouyejm.style.display="none";
	getjm.style.display="none";
	},false);
yinliao.addEventListener("click",function(){
	hanbaojm.style.display="none";
	yinliaojm.style.display="block";
	bqljm.style.display="none";
	zhajijm.style.display="none";
	shouyejm.style.display="none";
	getjm.style.display="none";
	},false);
bql.addEventListener("click",function(){
	hanbaojm.style.display="none";
	yinliaojm.style.display="none";
	bqljm.style.display="block";
	zhajijm.style.display="none";
	shouyejm.style.display="none";
	getjm.style.display="none";
	},false);
	
shouye.addEventListener("click",function(){
	hanbaojm.style.display="none";
	yinliaojm.style.display="none";
	bqljm.style.display="none";
	zhajijm.style.display="none";
	shouyejm.style.display="block";
	getjm.style.display="none";
	},false);
	
var shouyepic=document.getElementById("shouyepic");
var i=1;
var start=setInterval(piclb,2000);

function piclb(){
	if(i===3) i=0;
	i=i+1;
	shouyepic.src="image/首页"+i+".jpg";

	}
	
	
//时钟

var ctx=document.getElementById("cvs").getContext("2d");

drawtime();
setInterval(drawtime,10);


function drawtime(){
	ctx.clearRect(0,0,100,100);
	
	var date=new Date();
	var hour=date.getHours();
	var minute=date.getMinutes();
	var second=date.getSeconds();
	
	hour=hour>12? (hour%12) : hour ;
	
	hour+=minute/60;
	
	//画钟的外轮廓
	ctx.beginPath();
	ctx.lineWidth=3;
	ctx.strokeStyle="#B4B4B4";
	ctx.arc(50,50,40,0,2*Math.PI);
	ctx.stroke();
	ctx.closePath();
	
	//时刻度
	for(var i=1;i<=12;i++){
		ctx.save();
		ctx.beginPath();
		ctx.translate(50,50);
		ctx.rotate(i*30*Math.PI/180);
		ctx.moveTo(0,-35);
		ctx.lineTo(0,-39);
		ctx.stroke();
		ctx.restore();
	}
	
	//秒针
	ctx.save();
	ctx.beginPath();
	ctx.translate(50,50);
	ctx.rotate(second*(360*Math.PI/180)/60);
	ctx.moveTo(0,5);
	ctx.lineTo(0,-30);
	ctx.stroke();
	ctx.restore();
	
	//时针
	ctx.save();
	ctx.beginPath();
	ctx.translate(50,50);
	ctx.rotate(hour*(360*Math.PI/180)/12);
	ctx.moveTo(0,5);
	ctx.lineTo(0,-20);
	ctx.stroke();
	ctx.restore();

	}
	
//友好提示
var cfts=document.getElementById("cfts");
setInterval(ts,1000);
function ts(){
	var date=new Date();
	var hour=date.getHours();
	if(hour>=11&&hour<=1) cfts.innerHTML="上午好~";
	if(hour>=17&&hour<=19) cfts.innerHTML="下午好~";
	
	}

var login=document.getElementById("login");
var register=document.getElementById("register");
var tuichu=document.getElementById("tuichu");	

var sessio="<%= session.getAttribute("name")%>";

if(sessio!="null"){
	register.outerHTML="<a id='tuichu' href='delsession' style='text-decoration:none; color:#4F4F4F; '>退出</a>";
	login.outerHTML="<a id='login'  style='text-decoration:none; color:#E88602;'>"+sessio+"</a>";
	//login.innerHTML=sessio;
	}
	
//店长推荐、新闻中心、关于我们
var dztj=document.getElementById("dztj");
var xwzx=document.getElementById("xwzx");
var gywm=document.getElementById("gywm");
var gglnr1=document.getElementById("gglnr1");
var gglnr2=document.getElementById("gglnr2");
var gglnr3=document.getElementById("gglnr3");

dztj.addEventListener("mouseover",function(){
	gglnr1.style.display="block";
	gglnr2.style.display="none";
	gglnr3.style.display="none";
	dztj.style.borderBottom="2px solid white";
	xwzx.style.borderBottom="none";
	gywm.style.borderBottom="none";
	},false);
xwzx.addEventListener("mouseover",function(){
	gglnr2.style.display="block";
	gglnr1.style.display="none";
	gglnr3.style.display="none";
	xwzx.style.borderBottom="2px solid white";
	dztj.style.borderBottom="none";
	gywm.style.borderBottom="none";
	},false);
gywm.addEventListener("mouseover",function(){
	gglnr3.style.display="block";
	gglnr2.style.display="none";
	gglnr1.style.display="none";
	gywm.style.borderBottom="2px solid white";
	xwzx.style.borderBottom="none";
	dztj.style.borderBottom="none";
	},false);


//购物车单击事件

</script>

</html>
