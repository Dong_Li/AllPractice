<%-- 
    Document   : car
    Created on : 2018-6-19, 17:00:47
    Author     : Administrator
--%>

<%@page import="bean.PageBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="background-image:url(image/car1.jpg)">
        <%
           
            if(request.getSession().getAttribute("id") ==null) request.getRequestDispatcher("login.jsp").forward(request, response);
            float sum=0;
            PageBean pageBeann=(PageBean)request.getAttribute("pageBean");
            
            for(int i=0;i<pageBeann.getTotalRows();i++)
            {
                sum=sum+pageBeann.getData().get(i).getPrice();
            }
            
        %>
        
        <h2 align="center">购物车</h2>
        <table border="1" align="center" width="50%">
            <tr><th>序号</th><th>名字</th><th>价格</th></tr>
            <c:forEach var="menu" items="${pageBean.data}" varStatus="vs">
                <tr>
                    <td align="center"><c:out value="${vs.count}"/></td>
                    <td align="center"><c:out value="${menu.name}"/></td>
                    <td align="center"><c:out value="${menu.price}"/></td>
                </tr>
            </c:forEach>
        </table>
        
        <p align="center">
            每页${pageBean.pageSize}行
            共${pageBean.totalRows}行
            页数${pageBean.curPage}/${pageBean.totalPages}
            <br/>
            <c:choose>
                <c:when test="${pageBean.curPage==1}">首页 上一页</c:when>
                <c:otherwise>
                    <a href="carservlet?page=1">首页</a>
                    <a href="carservlet?page=${pageBean.curPage-1}">上一页</a>
                </c:otherwise>
            </c:choose>
                <c:choose>
                    <c:when test="${pageBean.curPage==pageBean.totalPages}">下一页 尾页</c:when>
                    <c:otherwise>
                        <a href="carservlet?page=${pageBean.curPage+1}">下一页</a>
                        <a href="carservlet?page=2">尾页</a>
                    </c:otherwise>
                </c:choose>
        </p>
        <form action="carsub">
            <input type="hidden" name="totalprice" value="<%= sum%>">
            <input type="hidden" name="totalRows" value="${pageBean.totalRows}">
            <center><input type="submit" value="提交订单"></center>
        </form>
        
        
    </body>
</html>
