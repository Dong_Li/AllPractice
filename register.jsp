
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>用户注册</title>
    </head>
<style>
body{
	background-color:#EAEAEA;
	}
#main{
	width:60%;
	height:600px;
	background-color:white;
	border:solid 1px #D4D4D4;
	border-radius:5px;
	margin:50px auto;
	}
#main form,table{
	margin:10px auto;
	}
#main tr{
	height:60px;
	}
#main input{
	height:20px
	}
#main th{
	text-align:right;
	}
#main table span{
	width:86px;
	height:50px;
	background-color:#D9D9D9;
	display:block;
	color:white;
	font-size:20px;
	float:left;
	text-align:center;
	line-height:50px;
	}
#main table span:hover{
	background-color:#00BB7E;
	cursor:pointer;
	}
</style>


    <body>
        <div id="main">
        	<div style="width:100%; height:50px; padding:0px; background-color:#00BB7E; border-bottom:solid 1px #CCCCCC;">
            	<img width="30" style=" display:block;float:left; margin-top:10px;margin-left:30px;" height="30" src="image/register1.png"><span style="color:white; font-weight:bold; display:block;float:left;font-size:18px; margin:10px 10px;">乡村基用户注册</span>
            </div>
            <form action="registercheck" method="post">
            	<table>
                	<tr><th>用户名：</th><td><input autocomplete="off" id="name" type="text" name="name"></td></tr>
                    <tr><th>密码：</th><td><input id="pw" type="password" name="pw"></td></tr>
                    <tr><th>性别：</th><td><input name="sex" id="sex" type="hidden" value="0"><span id="man">男</span><span id="woman">女</span></td></tr>
                    <tr><th>电话：</th><td><input autocomplete="off" id="tel" type="text" name="tel"></td></tr>
                    <tr><th>地址：</th><td><input autocomplete="off" id="addr" type="text" name="addr"></td></tr>
                    <tr><td colspan="2"><input id="tongyix" style=" margin-left:20px;height:18px; width:18px; vertical-align:middle;" onClick="tongyi(this)" type="checkbox">同意<a href="">服务条款</a></td></tr>
                    <tr><td colspan="2"><center><input style=" width:100px; height:30px;" id="sub" type="submit" disabled="disabled" value="提交"></center></td></tr>
                </table>
            </form>
        </div>
        
        <script>
		
		var man=document.getElementById("man");
		var woman=document.getElementById("woman");
		var sex=document.getElementById("sex");
		
		man.addEventListener("click",function(){
			sex.value=1;
			woman.style.backgroundColor="#D9D9D9";
			man.style.backgroundColor="#00BB7E";
			},false);
		woman.addEventListener("click",function(){
			sex.value=2;
			woman.style.backgroundColor="#00BB7E";
			man.style.backgroundColor="#D9D9D9";
			},false);
			
			
			<!--判空-->
			var name=document.getElementById("name");
			var pw=document.getElementById("pw");
			var tel=document.getElementById("tel");
			var addr=document.getElementById("addr");
			var sub=document.getElementById("sub");
			var tongyix=document.getElementById("tongyix");			
			function tongyi(){
				if(tongyix.checked){
					sub.disabled=false;
					}
					else{
						sub.disabled=true;
						}
				}			
			sub.addEventListener("click",function(){
				if(name.value===""||pw.value===""||tel.value===""||sex.value==="0"||addr.value==="") alert("信息不能为空！");
				},false);		
        </script>
    </body>
</html>
